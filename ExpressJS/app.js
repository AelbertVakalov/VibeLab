const express = require('express');
const app = express();
const port = 8080;

app.get('/main', (req, res) => {
  const ipAddress = req.ip;
  res.send('Главная страница, наш IP-адрес: ' + ipAddress);
});

app.get('/aboutUs', (req, res) => { 
  res.send('Как с нами связаться: vakalovaelbert@yandex.ru');
});

app.listen(port, () => {
  console.log('Сервер запущен!');
});
