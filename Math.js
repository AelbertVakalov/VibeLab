const addition = (a, b) => a + b;
const division = (a, b) => a / b;
const substraction = (a, b) => a - b;
const multiply = (a, b) => a * b;

const square = (a) => a * a;
const cube = (a) => a * a * a;

module.exports = {
    addition,
    division,
    substraction,
    multiply,
    square,
    cube,
};
