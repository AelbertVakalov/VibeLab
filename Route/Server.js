const express = require('express');
const app = express();
const port = 8080;

app.get('/', (req, res) => {
  
    res.send('Добро пожаловать на главную страницу!');
  });
  
  app.get('/about', (req, res) => {
    res.send('Это страница еще не заполнена, здесь будет информация о проекте.');
  });
  
  app.get('/contact', (req, res) => {
    res.send('Наши контактные данные: vakalovaelbert@yandex.ru');
  });
  
  app.use((req, res) => {
    res.status(404).send('Страница не найдена');
  });
  
  app.listen(port, () => {
    console.log(`Сервер запущен на порту ${port}`);
  });