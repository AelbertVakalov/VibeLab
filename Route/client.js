function sendRequest(url) {
    fetch(url, { 
        mode: 'no-cors'
})
      .then(response => response.text())
      .then(data => {
        document.getElementById('response').innerText = data;
      })
      .catch(error => {
        document.getElementById('response').innerText = 'Ошибка: ' + error.message;
      });
  }