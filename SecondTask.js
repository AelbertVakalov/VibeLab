const fs = require('fs-extra');

async function reader() {
  try {
    const data = await fs.readFile('Names.txt', 'utf-8');
    console.log(data);
  } catch (error) {
    console.error(error);
  }
}

reader();
console.log('Происходит чтение файла');