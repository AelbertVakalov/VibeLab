const myName = 'albert'
let age = 22;
const isDeveloper = true;
const dog = {
    nickname: 'bobik',
    isBarking: true,
    dogAge: 5,
    feed: ['Pedigree', 'Meat']
};

console.log(`
  Имя: ${myName},
  Я разработчик?: ${isDeveloper},
  Возраст: ${age},
  Моя собака: ${dog.nickname},
  Лает? ${dog.isBarking},
  Возраст собаки: ${dog.dogAge},
  Чем питается? ${dog.feed}
`);

const a = 10;
const b = 3;

const sum = a + b;
const difference = a - b;
const multiply = a * b;
const division = a / b;
const divisionRem = a % b;

console.log(`
Сумма: ${sum}, 
Разница: ${difference}, 
Произведение: ${multiply}, 
Частное: ${division}, 
Остаток: ${divisionRem}
`);

const truth = true;
const lie = false;

console.log(truth && lie, truth || lie, !truth, truth);

