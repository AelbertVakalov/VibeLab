const age = 20
if (age > 18){
    console.log(`Доступ разрешен, вам: ${age}`);
}

const salary = 100000;
switch(salary){
    case 5000:
        console.log('Нужно еще постараться!');
        break;
    case 30000:
        console.log('Уже лучше, но нужно еще постарарться');
        break;
    case 100000:
        console.log('Можно отдохнуть');
}

const countries = ['Америка' , 'Россия', 'Варшава', 'Грузия'];

for (let i = 0; countries.length > i; i++){
    console.log(countries[i]);
}

const fruits = ["яблоко", "банан", "апельсин", "киви", "груша"];

for (let fruit in fruits){
    console.log(fruits[fruit]);
}

const elements = [1, 2, 3, 4, 5, 6, 7];

for(let element of elements){
    console.log('Эллемент: ' + element);
}

let count = 0;
while (count < 5) {
  console.log("Значение count:", count);
  count++;
}


let num = 0;
do {
  console.log("Число: ", num);
  num++;
} while (num < 5);



function divideNumbers(a, b) {
    if (b === 0) {
      throw new Error('Деление на ноль недопустимо.');
    }
    return a / b;
  }
  

  try {
    const result = divideNumbers(10, 0);
    console.log('Результат:', result);
  } catch (error) {
    console.error('Ошибка:', error.message);
  }

  try {
    const result2 = divideNumbers(12, 4);
    console.log('Результат:', result2);
  } catch (error) {
    console.error('Ошибка:', error.message);
  }