function multiply(a,b){
    return a * b;
}

function sayHello(){
    console.log('Hello from JS');
}

sayHello();
console.log(multiply(5, 4));

const addition = (a, b) => a + b;
const sayGoodbye = () => console.log('Goodbye from JS');

sayGoodbye();
console.log(addition(5, 20));