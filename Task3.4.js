let mazda = {
    model: 'RX-7',
    age: 5,
    color: 'Red'
};

let toyota = {
    model: 'Supra',
    age : 12,
    color: 'Orange'
};

let nissan = {
    model: 'Skyline',
    age: 10,
    color: 'Green'
};

let subaru = {
    model: 'Impreza',
    age: 8,
    color: 'Orange'
};

let carArr = [mazda, toyota, nissan, subaru];

let redCar = carArr.filter(carArr => carArr.color == 'Orange');
let sortedCarByAge = carArr.sort((a, b) => a.age - b.age);
let oneCar = carArr.find(item => item.model == 'Impreza')

console.log(redCar);
console.log(oneCar);
console.log(sortedCarByAge);

