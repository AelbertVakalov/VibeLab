function delayPromise(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }
  
  function calculateSquare(num) {
    return new Promise((resolve) => {
      delayPromise(1000).then(() => {
        resolve(num * num);
      });
    });
  }
  
  function calculateCube(num) {
    return new Promise((resolve) => {
      delayPromise(1500).then(() => {
        resolve(num * num * num);
      });
    });
  }
  
  (async () => {
    try {
  
      const num = 5;
  
      const [squareResult, cubeResult] = await Promise.all([
        calculateSquare(num),
        calculateCube(num)
      ]);
  
      console.log(`Число: ${num}`);
      console.log("Квадрат числа:", squareResult);
      console.log("куб числа:", cubeResult);
  
      console.log("Конец операции.");
    } catch (error) {
      console.error("Ошибка:", error);
    }
  })();