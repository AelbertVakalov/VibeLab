const fs = require('fs');

function createFile(fileName, content) {
  fs.writeFile(fileName, content, (err) => {
    if (err) {
      console.error('Ошибка при создании файла:', err);
    } else {
      console.log('Файл успешно создан:', fileName);
    }
  });
}

function readFile(fileName) {
  fs.readFile(fileName, 'utf8', (err, data) => {
    if (err) {
      console.error('Ошибка при чтении файла:', err);
    } else {
      console.log('Содержимое файла', fileName, ':', data);
    }
  });
}

function updateFile(fileName, content) {
  fs.writeFile(fileName, content, { flag: 'a' }, (err) => {
    if (err) {
      console.error('Ошибка при обновлении файла:', err);
    } else {
      console.log('Файл успешно обновлен:', fileName);
    }
  });
}

function deleteFile(fileName) {
  fs.unlink(fileName, (err) => {
    if (err) {
      console.error('Ошибка при удалении файла:', err);
    } else {
      console.log('Файл успешно удален:', fileName);
    }
  });
}

const fileName = 'example.txt';
const fileContent = 'Пример текстового содержимого для файла.\n';

createFile(fileName, fileContent);
readFile(fileName);
updateFile(fileName, 'Новое содержимое файла.\n');
readFile(fileName);
deleteFile(fileName);
