const { add, multiply } = require('./math');

const result1 = add(5, 3);
const result2 = multiply(4, 6);

console.log(`Результат сложения: ${result1}`);
console.log(`Результат умножения: ${result2}`);
