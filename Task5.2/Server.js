const http = require('http');


const server = http.createServer((req, res) => {
  if (req.url === '/') {
    res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
    res.end('Привет! Это простой веб-сервер.');
    console.log('Запрос с главной страницы:', req.url);

  } else if (req.url === '/aboutUs') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    const aboutUsData = {
      email: 'info@example.com',
      phoneNumber: '123-456-7890',
    };

    res.end(JSON.stringify(aboutUsData));
    console.log('Запрос связанный c информацией o нас:', req.url);

  } else if(req.url === '/project') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    const project = {
      name: 'first try project on Node.js',
      hello: 'hello from node.js',
    }

    res.end(JSON.stringify(project));
    console.log('Запрос связанный c информацией o проекте:', req.url);
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Страница не найдена');
    console.log('Запрос получен, но страница не найдена:', req.url);
  }
});

server.listen(3000, () => {
  console.log('Сервер запущен на порту 3000');
});
