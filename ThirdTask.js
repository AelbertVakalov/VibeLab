const http = require('http');
const axios = require('axios');

function onRequest(req, res){
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.write('Hello world');
  res.end();
}


const PORT = 9000;
http.createServer(onRequest).listen(PORT, () => {
  console.log(`Сервер запущен`);

  
  const numRequests = 100;
  
  const serverUrl = `http://localhost:${PORT}`;


  async function makeRequests() {
    const startTime = Date.now();
    const requests = [];

    for (let i = 0; i < numRequests; i++) {
      requests.push(axios.get(serverUrl));
    }

    try {
      await Promise.all(requests);
      const endTime = Date.now();
      let totalTime = endTime - startTime;
      console.log(`Общее время на ${numRequests} запросов: ${totalTime} ms`);
    } catch (error) {
      console.error('Ошибка', error.message);
    }
  }

  makeRequests();
});