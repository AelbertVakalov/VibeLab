const express = require('express');
const app = express();

app.use((req, res, next) => {
  console.log(`[${new Date().toISOString()}] ${req.method} ${req.url}`);
  next();
});

app.use('/secured', (req, res, next) => {

  const token = req.headers.authorization;

  if (token === 'mysecrettoken') {
    next();
  } else {
    res.status(401).send('Unauthorized');
  }
});

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.get('/', (req, res) => {
  res.send('Привет, это главная страница!');
});

app.get('/secured/data', (req, res) => {
  res.send('Секретные данные');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Сервер запущен на порту ${PORT}`);
});
